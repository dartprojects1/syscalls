####### custom variables ######
DART=dart
PUB=${DART} pub

PROGRAM=./bin/syscalls.dart
PROGRAM_ARGS=

TEST_ARG_ARCH=
#TEST_ARG_ARCH=-a arm64
#TEST_ARG_ARCH=-a unknown

###### targets ######

#help
#help The main targets of this Makefile are:
#help
#help	help	this help
.PHONY: help
help:
	@sed -n 's/^#help//p' < Makefile

#help	analyze	Analyze the project with the specified analysis_options.yaml
.PHONY: analyze
analyze:
	@${DART} analyze

#help	fix	Fix all analysis errors that have an automated fix option
.PHONY: fix
fix:
	@${DART} fix --apply

#help	fmt	Format all dart files in the current directory
.PHONY:
fmt:
	@${DART} format .

#help	fix-all	Fix all analysis errors thay have an automated fix option and format all dart files in the current directory
.PHONY: fix-all
fix-all: fix fmt

#help	clean	Remove all build outputs
.PHONY: clean
clean:
	@rm -rf .dart_tool .packages build

#help	get-packages	Get all package dependencies
.PHONY: get-packages
get-packages:
	@${PUB} get

#help	build.yaml	Run the build_runner with the specified options from build.yaml
.PHONY: build.yaml
build.yaml:
	@${DART} run build_runner build

#help	run	Run the program
.PHONY: run
run:
	@${DART} run ${PROGRAM} ${PROGRAM_ARGS}

#help	run-help	Run the program with the --help flag
.PHONY: run-help
run-help:
	@${DART} run ${PROGRAM} --help

#help	test-all	Run the program with mode set to all
.PHONY: test-all
test-all:
	@${DART} run ${PROGRAM} -m all ${TEST_ARG_ARCH}

#help	test-name	Run the program with mode set to name
.PHONY: test-name
test-name:
	@${DART} run ${PROGRAM} -m name --name write ${TEST_ARG_ARCH}

#help	test-number	Run the program with mode set to number
.PHONY: test-number
test-number:
	@${DART} run ${PROGRAM} -m number --number 63 ${TEST_ARG_ARCH}

#help	test-conventions	Run the program with mode set to conventions
.PHONY: test-conventions
test-conventions:
	@${DART} run ${PROGRAM} -m conventions ${TEST_ARG_ARCH}

