if not AddOnPluginScriptsLoadedHandler then return end

AddOnPluginScriptsLoadedHandler(function()
  if not LspSetupDart then return end
  LspSetupDart()
end)
