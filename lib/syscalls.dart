library syscalls;

import "dart:async";
import "dart:convert";
import "dart:io";

import "package:http/http.dart" as http;
import "src/models.dart";

export "src/models.dart";

part "src/architecture.dart";
part "src/api.dart";
part "src/utils.dart";

Iterable<Syscall> _decodeSyscallBody(http.Response res) {
  return (jsonDecode(res.body) as List)
      .cast<Map<String, dynamic>>()
      .map(Syscall.fromJson);
}

/// Get all syscalls for [arch].
///
/// Throws an [ArgumentError] if [arch] is [Architecture.unknown].
Future<List<Syscall>?> getSyscallsAll(Architecture arch) async {
  _checkArch(arch);

  final res = await _getSyscalls(arch.name);
  if (HttpStatus.ok != res.statusCode) return null;

  return _decodeSyscallBody(res).toList();
}

/// Get the syscall for [arch] with the name [name].
///
/// Throws an [ArgumentError] if [arch] is [Architecture.unknown].
Future<Syscall?> getSyscallByName(
  Architecture arch,
  String name,
) async {
  _checkArch(arch);

  final res = await _getSyscalls(name);
  if (HttpStatus.ok != res.statusCode) return null;

  return _decodeSyscallBody(res).firstWhere((e) => e.arch == arch);
}

/// Get the syscall for [arch] with the number [number].
///
/// Throws an [ArgumentError] if [arch] is [Architecture.unknown].
Future<Syscall?> getSyscallByNumber(
  Architecture arch,
  int number,
) async {
  _checkArch(arch);

  final res = await _getSyscalls("$number");
  if (HttpStatus.ok != res.statusCode) return null;

  return _decodeSyscallBody(res).firstWhere((e) => e.arch == arch);
}

/// Get the convention for [arch].
///
/// If [arch] is [Architecture.unknown], then the conventions for
/// all platforms are returned.
Future<Convention?> getConvention(Architecture arch) async {
  _checkArch(arch);

  final res = await _getConventions(arch.name);
  if (HttpStatus.ok != res.statusCode) return null;

  return Convention.fromJson(jsonDecode(res.body));
}

Future<List<Convention>?> getConventionsAll() async {
  final res = await _getConventions("");
  if (HttpStatus.ok != res.statusCode) return null;

  return (jsonDecode(res.body) as List)
      .cast<Map<String, dynamic>>()
      .map(Convention.fromJson)
      .toList();
}

/// Check if [arch] is not [Architecture.unknown],
/// otherwise throw an [ArgumentError].
void _checkArch(Architecture arch) {
  if (arch.isKnown) return;

  throw ArgumentError.value(
    arch.toString(),
    "arch",
    "The architecture cannot be unknown if you want to "
        "get a list of all system calls",
  );
}
