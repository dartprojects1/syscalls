// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Syscall _$SyscallFromJson(Map<String, dynamic> json) => Syscall._(
      arch: _parseArch(json['arch'] as String),
      nr: (json['nr'] as num).toInt(),
      name: json['name'] as String,
      references: json['refs'] as String,
      callNumber: int.parse(json['return'] as String),
      arg0: _parseSyscallArg(json['arg0'] as String),
      arg1: _parseSyscallArg(json['arg1'] as String),
      arg2: _parseSyscallArg(json['arg2'] as String),
      arg3: _parseSyscallArg(json['arg3'] as String),
      arg4: _parseSyscallArg(json['arg4'] as String),
      arg5: _parseSyscallArg(json['arg5'] as String),
    );

Map<String, dynamic> _$SyscallToJson(Syscall instance) => <String, dynamic>{
      'arch': _$ArchitectureEnumMap[instance.arch]!,
      'nr': instance.nr,
      'name': instance.name,
      'refs': instance.references,
      'return': _toHex(instance.callNumber),
      'arg0': instance.arg0,
      'arg1': instance.arg1,
      'arg2': instance.arg2,
      'arg3': instance.arg3,
      'arg4': instance.arg4,
      'arg5': instance.arg5,
    };

const _$ArchitectureEnumMap = {
  Architecture.unknown: 'unknown',
  Architecture.arm: 'arm',
  Architecture.arm64: 'arm64',
  Architecture.x64: 'x64',
  Architecture.x86: 'x86',
};

Convention _$ConventionFromJson(Map<String, dynamic> json) => Convention._(
      arch: _parseArch(json['arch'] as String),
      nr: json['nr'] as String,
      callNumber: json['return'] as String,
      arg0: json['arg0'] as String,
      arg1: json['arg1'] as String,
      arg2: json['arg2'] as String,
      arg3: json['arg3'] as String,
      arg4: json['arg4'] as String,
      arg5: json['arg5'] as String,
    );

Map<String, dynamic> _$ConventionToJson(Convention instance) =>
    <String, dynamic>{
      'arch': _$ArchitectureEnumMap[instance.arch]!,
      'nr': instance.nr,
      'return': instance.callNumber,
      'arg0': instance.arg0,
      'arg1': instance.arg1,
      'arg2': instance.arg2,
      'arg3': instance.arg3,
      'arg4': instance.arg4,
      'arg5': instance.arg5,
    };
