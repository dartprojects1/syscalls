import "package:equatable/equatable.dart";
import "package:json_annotation/json_annotation.dart";

import "../syscalls.dart";

part "models.g.dart";

Architecture _parseArch(String json) {
  return str2enum(json, Architecture.values);
}

String? _parseSyscallArg(String json) {
  return json.isEmpty ? null : json;
}

String _toHex(int value) {
  return "0x${value.toRadixString(16).toUpperCase().padLeft(2, '0')}";
}

abstract base class EquatableModel extends Equatable {
  const EquatableModel();

  Map<String, dynamic> toJson();

  List<dynamic> toTableLine() {
    final line = _toTableLineImpl();

    if (10 != line.length)
      throw FormatException(
        "A table line must contain 10 elements, but this line "
        "only has ${line.length}",
        line,
      );

    return line;
  }

  List<dynamic> _toTableLineImpl();
}

@JsonSerializable(constructor: "_")
final class Syscall extends EquatableModel {
  const Syscall._({
    required this.arch,
    required this.nr,
    required this.name,
    required this.references,
    required this.callNumber,
    required this.arg0,
    required this.arg1,
    required this.arg2,
    required this.arg3,
    required this.arg4,
    required this.arg5,
  }) : assert(Architecture.unknown != arch);

  factory Syscall.fromJson(Map<String, dynamic> json) =>
      _$SyscallFromJson(json);

  @JsonKey(fromJson: _parseArch)
  final Architecture arch;

  final int nr;

  final String name;

  @JsonKey(name: "refs")
  final String references;

  @JsonKey(name: "return", fromJson: int.parse, toJson: _toHex)
  final int callNumber;

  @JsonKey(fromJson: _parseSyscallArg)
  final String? arg0;

  @JsonKey(fromJson: _parseSyscallArg)
  final String? arg1;

  @JsonKey(fromJson: _parseSyscallArg)
  final String? arg2;

  @JsonKey(fromJson: _parseSyscallArg)
  final String? arg3;

  @JsonKey(fromJson: _parseSyscallArg)
  final String? arg4;

  @JsonKey(fromJson: _parseSyscallArg)
  final String? arg5;

  @override
  List<Object?> get props => [
        arch,
        nr,
        callNumber,
        arg0,
        arg1,
        arg2,
        arg3,
        arg4,
        arg5,
      ];

  @override
  Map<String, dynamic> toJson() => _$SyscallToJson(this);

  @override
  List<dynamic> _toTableLineImpl() {
    return [
      nr,
      name,
      references,
      _toHex(callNumber),
      arg0 ?? "",
      arg1 ?? "",
      arg2 ?? "",
      arg3 ?? "",
      arg4 ?? "",
      arg5 ?? "",
    ];
  }
}

@JsonSerializable(constructor: "_")
final class Convention extends EquatableModel {
  const Convention._({
    required this.arch,
    required this.nr,
    required this.callNumber,
    required this.arg0,
    required this.arg1,
    required this.arg2,
    required this.arg3,
    required this.arg4,
    required this.arg5,
  }) : assert(Architecture.unknown != arch);

  factory Convention.fromJson(Map<String, dynamic> json) =>
      _$ConventionFromJson(json);

  @JsonKey(fromJson: _parseArch)
  final Architecture arch;

  final String nr;

  @JsonKey(name: "return")
  final String callNumber;

  final String arg0;

  final String arg1;

  final String arg2;

  final String arg3;

  final String arg4;

  final String arg5;

  @override
  Map<String, dynamic> toJson() => _$ConventionToJson(this);

  @override
  List<Object?> get props => [
        arch,
        nr,
        callNumber,
        arg0,
        arg1,
        arg2,
        arg3,
        arg4,
        arg5,
      ];

  @override
  List<dynamic> _toTableLineImpl() {
    return [
      "NR ($nr)",
      "NAME",
      "REFERENCES",
      "RETURN ($callNumber)",
      "ARG0 ($arg0)",
      "ARG1 ($arg1)",
      "ARG2 ($arg2)",
      "ARG3 ($arg3)",
      "ARG4 ($arg4)",
      "ARG5 ($arg5)",
    ];
  }
}
