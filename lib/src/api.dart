part of "../syscalls.dart";

/// [See](https://api.syscall.sh/swagger/index.html)

const _apiScheme = "https";
const _apiHost = "api.syscall.sh";
const _apiVersion = "v1";

Future<http.Response> _getSyscalls(String path) {
  var _path = "syscalls";
  if (path.isNotEmpty) {
    _path += "/$path";
  }
  return _get(_path);
}

Future<http.Response> _getConventions(String path) {
  var _path = "conventions";
  if (path.isNotEmpty) {
    _path += "/$path";
  }
  return _get(_path);
}

Future<http.Response> _get(String path) {
  assert(path.isNotEmpty);
  final uri = Uri(
    scheme: _apiScheme,
    host: _apiHost,
    path: "$_apiVersion/$path",
  );
  return http.get(uri);
}
