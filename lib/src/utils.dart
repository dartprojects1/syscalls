part of "../syscalls.dart";

T str2enum<T extends Enum>(
  String s,
  List<T> values,
) {
  final idx = values
      .map((e) => e.name)
      .toList(growable: false)
      .indexWhere((e) => e == s);
  return values[idx];
}
