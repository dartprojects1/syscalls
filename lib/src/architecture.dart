part of "../syscalls.dart";

enum Architecture implements Comparable<Architecture> {
  unknown,
  arm,
  arm64,
  x64,
  x86;

  @override
  int compareTo(Architecture other) => other.index - index;

  bool get isUnknown => Architecture.unknown == this;
  bool get isKnown => !isUnknown;

  Set<String> get alternativeNames {
    switch (this) {
      case Architecture.unknown:
        return {};

      case Architecture.arm:
        return {
          "aarch32",
        };

      case Architecture.arm64:
        return {
          "aarch64",
          "arm64",
        };

      case Architecture.x64:
        return {
          "x64",
          "x86_64",
          "AMD64",
          "Intel 64",
        };

      case Architecture.x86:
        return {
          "x86",
        };
    }
  }
}

/// Get the [Architecture] of the cpu of the device this script is run on.
Future<Architecture> getCpuArch() async {
  final String? cpu;
  if (Platform.isWindows) {
    cpu = Platform.environment["PROCESSOR_ARCHITECTURE"];
  } else {
    final info = await Process.run("uname", ["-m"]);
    cpu = info.stdout.toString().replaceAll("\n", "");
  }
  if (null == cpu) return Architecture.unknown;

  return Architecture.values.firstWhere(
    (element) => element.alternativeNames.contains(cpu),
    orElse: () => Architecture.unknown,
  );
}
