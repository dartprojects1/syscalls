import "dart:convert";
import "dart:io";

import "package:args/args.dart";
import "package:cli_table/cli_table.dart";
import "package:syscalls/pubspec.g.dart" as pubs;
import "package:syscalls/syscalls.dart";

Future<void> main(List<String> args) async {
  final cpuArch = await getCpuArch();

  final parser = ArgParser()
    ..addOption(
      "mode",
      abbr: "m",
      allowed: Mode.values.map((e) => e.name),
      help: "The mode used for requesting the syscalls.",
      defaultsTo: Mode.conventions.name,
    )
    ..addOption(
      "arch",
      abbr: "a",
      allowed: Architecture.values.map((e) => e.name),
      help: "The architecture for which you want to get "
          "information about the system calls.\n"
          "Defaults to the architecture of the current system.",
      defaultsTo: cpuArch.name,
    )
    ..addOption(
      "name",
      valueHelp: "write",
      help: "The name of the system call. Must be specified "
          "if mode is set to '${Mode.name.name}'.",
    )
    ..addOption(
      "number",
      valueHelp: "64",
      help: "The number of the system call. Must be specified "
          "if mode is set to '${Mode.number.name}'.",
    )
    ..addOption(
      "format",
      abbr: "f",
      help: "The output format for the retrieved data.",
      allowed: OutputFormat.values.map((e) => e.name),
      defaultsTo: OutputFormat.table.name,
    )
    ..addFlag(
      "help",
      abbr: "h",
      negatable: false,
      help: "Print this message.",
      defaultsTo: false,
    );

  final res = parser.parse(args);

  // Print help message.
  if (res["help"]) {
    // ignore: avoid_print
    print(
      "v${pubs.version}\n"
      "${pubs.name} - ${pubs.description}\n\n"
      "${parser.usage}",
    );
    exit(0);
  }

  // Extract values from res.
  final mode = str2enum(res["mode"], Mode.values);
  final arch = str2enum(res["arch"], Architecture.values);
  final format = str2enum(res["format"], OutputFormat.values);

  switch (mode) {
    case Mode.all:
      final calls = await getSyscallsAll(arch);
      if (null == calls) _fail(code: 1, arch: arch);

      final conv = await getConvention(arch);
      if (null == conv) _fail(code: 1, arch: arch);

      printSyscalls(conv, calls, format);
      break;

    case Mode.name:
      ArgumentError.checkNotNull(res["name"], "name");

      final call = await getSyscallByName(arch, res["name"]);
      if (null == call) _fail(code: 1, arch: arch);

      final conv = await getConvention(arch);
      if (null == conv) _fail(code: 1, arch: arch);

      printSyscall(conv, call, format);
      break;

    case Mode.number:
      ArgumentError.checkNotNull(res["number"], "number");

      final call = await getSyscallByNumber(arch, int.parse(res["number"]));
      if (null == call) _fail(code: 1, arch: arch);

      final conv = await getConvention(arch);
      if (null == conv) _fail(code: 1, arch: arch);

      printSyscall(conv, call, format);
      break;

    case Mode.conventions:
      final convs = await getConventionsAll();
      if (null == convs) _fail(code: 1, arch: arch);

      printConventions(convs, format);
      break;
  }
}

Never _fail({
  required int code,
  required Architecture arch,
  String? message,
}) {
  var _message = "Failure";
  if (null != message) {
    _message += "\nmessage: $message";
  }

  stderr.writeln(_message);
  exit(code);
}

//

void printSyscall(
  Convention convention,
  Syscall call,
  OutputFormat format,
) {
  print(
    _formatModels(
      [convention, call],
      format,
      const OutputFormatOptions(
        emphasizeFirst: true,
      ),
    ),
  );
}

void printSyscalls(
  Convention convention,
  List<Syscall> calls,
  OutputFormat format,
) {
  calls.sort((a,b) => a.nr.compareTo(b.nr));
  print(
    _formatModels(
      [convention, ...calls],
      format,
      const OutputFormatOptions(
        emphasizeFirst: true,
      ),
    ),
  );
}

void printConventions(
  List<Convention> convs,
  OutputFormat format,
) {
  print(
    _formatModels(
      convs,
      format,
    ),
  );
}

//

enum Mode {
  all,
  name,
  number,
  conventions,
}

enum OutputFormat {
  json,
  prettyJson,
  table,
}

final class OutputFormatOptions {
  const OutputFormatOptions({
    this.emphasizeFirst = false,
  });

  final bool emphasizeFirst;
}

String _formatModels(
  dynamic input,
  OutputFormat format, [
  OutputFormatOptions options = const OutputFormatOptions(),
]) {
  final theInput = switch (input) {
    EquatableModel() => [input],
    Iterable<EquatableModel>() => input,
    _ => throw ArgumentError.value(
        input,
        "input",
        "Must be of type EquatableModel or an Iterable of EquatableModel",
      )
  };

  assert(theInput.isNotEmpty);

  switch (format) {
    case OutputFormat.json:
      return jsonEncode(theInput);

    case OutputFormat.prettyJson:
      return const JsonEncoder.withIndent("  ").convert(theInput);

    case OutputFormat.table:
      final Table tbl;
      final iter = theInput.iterator;

      if (options.emphasizeFirst) {
        iter.moveNext();
        tbl = Table(header: iter.current.toTableLine());
      } else {
        tbl = Table();
      }

      while (iter.moveNext()) {
        tbl.add(iter.current.toTableLine());
      }

      return tbl.toString();
  }
}
