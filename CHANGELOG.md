# 2.1.0

  * Added output format `table`.

  * Fixed API interface.

# 2.0.0

  * Migrated to dart sdk `>= 3.0.0 < 4.0.0` 

# 1.0.0

  * Initial version.
